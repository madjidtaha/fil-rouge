
namespace 'Application', (exports) ->

    class exports.SpriteController

        canvas : null

        context : null

        voice : false

        spriteSinger : [null, null]

        spriteGuitarist : [null, null, null, null]

        spriteVoice : [null, null]

        raf: null

        time: null

        constructor: ()->

            console.log 'constructor ::: SpriteController'

            @canvas = document.getElementById 'sprite'

            @canvas.width = window.innerWidth

            @canvas.height = window.innerHeight

            @context = @canvas.getContext '2d'

            @fillSprite()

            @addEventListener()
        
            # @context.fillStyle = '#000000'

            # @context.fillRect 0, 0, window.innerWidth, window.innerHeight 

        addEventListener: ()=>

            $(window).on 'resize', @onResize

        onResize:()=>

            @canvas.width = window.innerWidth

            @canvas.height = window.innerHeight


        init: ()=>

            console.log 'init ::: SpriteController'

            @time = Date.now()

            @raf = requestAnimationFrame @update


        fillSprite: ()=>

            firstSpriteGuitarist = new Image()

            firstSpriteGuitarist.src = 'images/Guitarist_01.png'

            firstSpriteGuitarist.onload = ()=>

                @spriteGuitarist[0] = firstSpriteGuitarist

            secondSpriteGuitarist = new Image()

            secondSpriteGuitarist.src = 'images/Guitarist_02.png'

            secondSpriteGuitarist.onload = ()=>

                @spriteGuitarist[1] = secondSpriteGuitarist


            firstSpriteSinger = new Image()

            firstSpriteSinger.src = 'images/Singer_01.png'

            firstSpriteSinger.onload = ()=>

                @spriteSinger[0] = firstSpriteSinger

            secondSpriteSinger = new Image()

            secondSpriteSinger.src = 'images/Singer_02.png'

            secondSpriteSinger.onload = ()=>

                @spriteSinger[1] = secondSpriteSinger


            firstSpriteVoice = new Image()

            firstSpriteVoice.src = 'images/Voice_01.png'

            firstSpriteVoice.onload = ()=>

                @spriteVoice[0] = firstSpriteVoice

            secondSpriteVoice = new Image()

            secondSpriteVoice.src = 'images/Voice_02.png'

            secondSpriteVoice.onload = ()=>

                @spriteVoice[1] = secondSpriteVoice

            thirdSpriteVoice = new Image()

            thirdSpriteVoice.src = 'images/Voice_03.png'

            thirdSpriteVoice.onload = ()=>

                @spriteVoice[2] = thirdSpriteVoice

            fourthSpriteVoice = new Image()

            fourthSpriteVoice.src = 'images/Voice_04.png'

            fourthSpriteVoice.onload = ()=>

                @spriteVoice[3] = fourthSpriteVoice


        update: ()=>

            @context.clearRect 0, 0, window.innerWidth, window.innerHeight 

            if Date.now() - @time > 500

                @time = Date.now()

            voiceSprite = unless @voice then 0 else 1

            if Date.now() - @time < 250

                @context.drawImage @spriteVoice[voiceSprite], window.innerWidth-100, window.innerHeight-200

                @context.drawImage @spriteVoice[voiceSprite], window.innerWidth-150, window.innerHeight-250

                @context.drawImage @spriteVoice[voiceSprite], window.innerWidth-200, window.innerHeight-300
                
                @context.drawImage @spriteSinger[0], window.innerWidth/2-27, window.innerHeight-300

                @context.drawImage @spriteGuitarist[0], 50, window.innerHeight-200

                @context.drawImage @spriteGuitarist[0], 120, window.innerHeight-250

                @context.drawImage @spriteGuitarist[0], 190, window.innerHeight-300

            else

                @context.drawImage @spriteVoice[voiceSprite+1], window.innerWidth-100, window.innerHeight-200

                @context.drawImage @spriteVoice[voiceSprite+1], window.innerWidth-150, window.innerHeight-250

                @context.drawImage @spriteVoice[voiceSprite+1], window.innerWidth-200, window.innerHeight-300

                @context.drawImage @spriteSinger[1], window.innerWidth/2-27, window.innerHeight-300

                @context.drawImage @spriteGuitarist[1], 50, window.innerHeight-200

                @context.drawImage @spriteGuitarist[1], 120, window.innerHeight-250

                @context.drawImage @spriteGuitarist[1], 190, window.innerHeight-300




            @raf = requestAnimationFrame @update
