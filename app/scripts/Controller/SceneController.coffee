
namespace 'Application', (exports) ->

    class exports.SceneController

        canvas : null

        context : null

        constructor: ()->

            console.log 'constructor ::: SceneController'

            @canvas = document.getElementById 'canvas'

            @canvas.width = window.innerWidth

            @canvas.height = window.innerHeight

            @context = @canvas.getContext '2d'

            @drawScene()

        kickOn: ()=>

            @context.scale(1, 1);

            @context.fillStyle = '#FFFFFF'

            @context.fillRect 0, 0, window.innerWidth, window.innerHeight 


        kickOff: ()=>

            @drawScene()

        drawScene: ()=>

            @context.fillStyle = '#0E5F32'

            @context.fillRect 0, 0, window.innerWidth, window.innerHeight 

            @context.fillStyle = '#CCCCCC'


            i=0
            while i*window.innerWidth/5 < window.innerWidth

                @context.fillRect i*window.innerWidth/5, 0, 100, window.innerHeight 

                i++

            @context.fillStyle = '#FFFFFF'

            @context.fillRect 0, window.innerHeight/3*2, window.innerWidth, window.innerHeight/3
            
            @context.save()

            @context.scale(1, 0.3);

            @context.beginPath();
            
            @context.arc(window.innerWidth / 2, window.innerHeight*2.25, 180, 0, 2 * Math.PI);
            
            @context.fill();

            @context.closePath();

            @context.restore()


        firstKick: ()=>

            console.log 'firstKick'

            @context.font = "60pt uni0553";
            # @context.strokeStyle = "rgb(200,100,0)";
            @context.fillStyle = "rgb(255,255,255)";
            @context.strokeText "TAM", 65, 100;
            @context.fillText "TAM", 65, 100;

        secondKick: ()=>

            console.log 'secondKick'

            @context.font = "60pt uni0553";
            # @context.strokeStyle = "rgb(200,100,0)";
            @context.fillStyle = "rgb(255,255,255)";
            @context.strokeText "TAM", window.innerWidth/3+65, 100;
            @context.fillText "TAM", window.innerWidth/3+65, 100;

        thirdKick: ()=>

            console.log 'thirdKick'

            @context.font = "60pt uni0553";
            # @context.strokeStyle = "rgb(200,100,0)";
            @context.fillStyle = "rgb(255,255,255)";
            @context.strokeText "TAM", window.innerWidth/3*2+65, 100;
            @context.fillText "TAM", window.innerWidth/3*2+65, 100;


   

