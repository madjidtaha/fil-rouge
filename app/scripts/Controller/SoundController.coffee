
namespace 'Application', (exports) ->

    class exports.SoundController

        audio : null

        audioOriginal : null

        dancer : null

        dancerOriginal: null

        raf : null

        kik : null

        chiptune : true

        mute : false

        constructor: ()->

            console.log 'constructor ::: SoundController'

            @audio = document.getElementById('audio')

            @audioOriginal = document.getElementById('audioOriginal')


            @dancer = new Dancer

            @dancerOriginal = new Dancer

            @dancerOriginal.load @audioOriginal

            # @dancer.play()


            @kick = @dancer.createKick

                onKick: (mag) =>

                    console.log('Kick!');

                    App.sceneController.kickOn()

                offKick: (mag) =>

                    console.log('no kick :(')

                    App.sceneController.kickOff()

            # @kick.on()

            @dancer.onceAt(0, =>

                console.log "-- 0", @dancer.getTime()

                TweenMax.to $('#text-1 .word-1'), .2,
                    autoAlpha: 1
                    scale: 1

                TweenMax.to $('#text-1 .word-2'), .2,
                    autoAlpha: 1
                    scale: 1
                    delay: .3

                TweenMax.to $('#text-1 .word-3'), .2,
                    autoAlpha: 1
                    scale: 1
                    delay: .6
                    onComplete: =>

                        TweenMax.to $('#text-1').html('Go !')

                        TweenMax.to $('#text-1'), .5,

                            scale: 10
                            autoAlpha: 0
                            onComplete:()=>
                                $('#text-1').hide()

            ).onceAt(1.5, => 

                console.log "-- 1.5", @dancer.getTime()
                
                $('#text-2 .line-1').show()

                TweenMax.to $('#text-2 .line-1'), .3,
                    autoAlpha: 1


            ).between(1.5, 3.5, =>
                # console.log "#text-2 line-1", @dancer.getTime()
            ).onceAt(3.5, =>

                console.log "-- 3.5", @dancer.getTime()

                TweenMax.to $('#text-2 .line-1'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-2 .line-1').hide()

                        $('#text-2 .line-2').show()

                        TweenMax.to $('#text-2 .line-2'), .3,
                            autoAlpha: 1


            ).onceAt(5, =>

                console.log "-- 5", @dancer.getTime()

                TweenMax.to $('#text-2 .line-2'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-2 .line-2').hide()

                        $('#text-2 .line-3').show()

                        TweenMax.to $('#text-2 .line-3'), .3,
                            autoAlpha: 1


            # ).between(7, 9, =>
            #     console.log "-- 9, 11", @dancer.getTime()
            #     console.log "--- ", Math.floor(@dancer.getTime() % 3)

            #     while @dancer.getTime() < 7.2 then App.sceneController.firstKick()
            #     while @dancer.getTime() < 7.9 then App.sceneController.secondKick()
            #     while @dancer.getTime() < 8.9 then App.sceneController.thirdKick()
                               

            ).onceAt(9.5, =>

                console.log "-- 9.5", @dancer.getTime()

                TweenMax.to $('#text-2'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-2').hide()

                        $('#text-3 .line-1').show()

                        TweenMax.to $('#text-3 .line-1'), .3,
                            autoAlpha: 1


            ).onceAt(12, =>

                console.log "-- 12", @dancer.getTime()

                TweenMax.to $('#text-3 .line-1'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-3 .line-1').hide()

                        $('#text-3 .line-2').show()

                        TweenMax.to $('#text-3 .line-2'), .3,
                            autoAlpha: 1


            ).onceAt(13.5, =>

                console.log "-- 13.5", @dancer.getTime()

                TweenMax.to $('#text-3 .line-2'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-3 .line-2').hide()

                        $('#text-3 .line-3').show()

                        TweenMax.to $('#text-3 .line-3'), .3,
                            autoAlpha: 1


            ).onceAt(18, =>

                console.log "-- 18", @dancer.getTime()

                TweenMax.to $('#text-3'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-3').hide()

                        $('#text-4 .line-1').show()

                        TweenMax.to $('#text-4 .line-1'), .3,
                            autoAlpha: 1


            ).onceAt(20.5, =>

                console.log "-- 20.5", @dancer.getTime()

                TweenMax.to $('#text-4 .line-1'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-4 .line-1').hide()

                        $('#text-4 .line-2').show()

                        TweenMax.to $('#text-4 .line-2'), .3,
                            autoAlpha: 1


            ).onceAt(22, =>

                console.log "-- 22", @dancer.getTime()

                TweenMax.to $('#text-4 .line-2'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-4 .line-2').hide()

                        $('#text-4 .line-3').show()

                        TweenMax.to $('#text-4 .line-3'), .3,
                            autoAlpha: 1


            ).onceAt(26.5, =>

                console.log "-- 26.5", @dancer.getTime()

                TweenMax.to $('#text-4'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-4').hide()

                        $('#text-5 .line-1').show()

                        TweenMax.to $('#text-5 .line-1'), .3,
                            autoAlpha: 1


            ).onceAt(28.5, =>

                console.log "-- 28.5", @dancer.getTime()

                TweenMax.to $('#text-5 .line-1'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-5 .line-1').hide()

                        $('#text-5 .line-2').show()

                        TweenMax.to $('#text-5 .line-2'), .3,
                            autoAlpha: 1


            ).onceAt(30.5, =>

                console.log "-- 30.5", @dancer.getTime()

                TweenMax.to $('#text-5 .line-2'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-5 .line-2').hide()

                        $('#text-5 .line-3').show()

                        TweenMax.to $('#text-5 .line-3'), .3,
                            autoAlpha: 1


            ).onceAt(34.5, =>

                console.log "-- 34.5", @dancer.getTime()

                TweenMax.to $('#text-5'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#text-5').hide()

                        $('#hook .line-1').show()

                        TweenMax.to $('#hook .line-1'), .3,
                            autoAlpha: 1

                App.spriteController.voice = true;
                @kick.on()

            ).between(34.5, 67.5, =>
                console.log "-- 34.5, 67.5", @dancer.getTime()

                $('#hook .line-1 .text-0').css('position', 'relative')
                $('#hook .line-1 .text-1').css('position', 'relative')

                TweenMax.set $('#hook .line-1 .text-0'),
                    top: _.random(-2,2)

                TweenMax.set $('#hook .line-1 .text-1'),
                    top: _.random(-2,2)


                # console.log "#text-2 line-1", @dancer.getTime()
            ).onceAt(67.5, =>

                console.log "-- 34.5", @dancer.getTime()

                TweenMax.to $('#hook'), .1,
                    autoAlpha: 0
                    onComplete: =>
                        $('#hook').hide()

                        # $('#hook .line-1').show()

                        # TweenMax.to $('#hook .line-1'), .3,
                        #     autoAlpha: 1

                @kick.off()

            ).load @audio


        init: ()=>

            unless @mute

                if @chiptune

                    @dancer.setVolume '1'

                    @dancerOriginal.setVolume '0'

                else

                    @dancer.setVolume '0'

                    @dancerOriginal.setVolume '1'

            else

                @dancer.setVolume '0'

                @dancerOriginal.setVolume '0'



            @dancer.play()

            @dancerOriginal.play()

            @raf = requestAnimationFrame @update



        update: ()=>

            unless @dancer.getTime() >= 234.919184

                @raf = requestAnimationFrame @update

            else

                @kick.off()
                
                cancelAnimationFrame @raf


        toggle8bit: =>

            console.log('toggle');

            TweenMax.killTweensOf(@dancer)
            TweenMax.killTweensOf(@dancerOriginal)

            if @chiptune

                $('#switcher').removeClass('on')
                $('#switcher').addClass('off')


                unless @mute

                    TweenMax.to @dancer, 1, 
                        setVolume: 0
                    TweenMax.to @dancerOriginal, 1, 
                        setVolume: 1

            else

                $('#switcher').removeClass('off')
                $('#switcher').addClass('on')


                unless @mute

                    TweenMax.to @dancer, 1, 
                        setVolume: 1
                    TweenMax.to @dancerOriginal, 1, 
                        setVolume: 0




            @chiptune = !@chiptune