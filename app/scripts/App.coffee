namespace = (target, name, block) ->
    [target, name, block] = [(if typeof exports isnt 'undefined' then exports else window), arguments...] if arguments.length < 3
    top = target
    target = target[item] or= {} for item in name.split '.'
    block target, top

###*
* APP
###

namespace 'window', (exports) ->

    class exports.App

        soundController : null

        constructor: ()->

            console.log 'constructor ::: App'

            @soundController = new Application.SoundController

            @sceneController = new Application.SceneController

            @spriteController = new Application.SpriteController

            @prepareDOM()

            @addEventListener()

        addEventListener: =>

            $('#switcher').on 'click', @soundController.toggle8bit
            $('#start').on 'click', @start

        start: =>

            @spriteController.init()

            TweenMax.to $('#splash'), .5,
                autoAlpha: 0
                onComplete: ()=>

                    $('#switcher').show()
                    TweenMax.from $('#switcher'), .5, 
                        autoAlpha: 0

                    @soundController.init()

        prepareDOM: ()=>

            i=2
            while i < 6
                j=1
                while j < 4 
                    line = $("#text-#{i} .line-#{j}").text().split(" ");
                    $("#text-#{i} .line-#{j}").empty()

                    $.each(line, (k, v)=>
                        $("#text-#{i} .line-#{j}").append($("<span class='text-#{k}'>").text(v))
                        $("#text-#{i} .line-#{j}").append(' ')
                    )

                    TweenMax.set $("#text-#{i} .line-#{j}"), 
                        autoAlpha: 0

                    $("#text-#{i} .line-#{i}").hide()
                    j++
                i++

            i=1
            while i < 9
                line = $("#hook .line-#{i}").text().split(" ");
                $("#hook .line-#{i}").empty()

                $.each(line, (j, v)=>
                    $("#hook .line-#{i}").append($("<span class='text-#{j}'>").text(v))
                    $("#hook .line-#{i}").append(' ')                 
                )

                TweenMax.set $("#hook .line-#{i}"), 
                    autoAlpha: 0

                $("#hook .line-#{i}").hide()

                i++

$ ->

    $(document).ready ->

        window.App = new App
